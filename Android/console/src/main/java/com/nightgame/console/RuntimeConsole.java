package com.nightgame.console;

import android.util.Log;

import com.unity3d.player.UnityPlayer;

public class RuntimeConsole
{
    private ConsoleLogcat _Logcat;

    private static final String _ObjName = "RuntimeConsole";
    private static final String _MethodName = "OnMessage";

    private IConsoleReceive _IConsoleReceive = new IConsoleReceive()
    {
        @Override
        public void OnLogReceived(String str)
        {
            UnityPlayer.UnitySendMessage(_ObjName, _MethodName, str);
        }
    };

    public void start()
    {
        Log.d("Unity","Android start");
        stop();

        _Logcat = new ConsoleLogcat(_IConsoleReceive);

        Thread thread = new Thread(_Logcat);
        thread.start();
    }


    public void stop()
    {
        Log.d("Unity","Android stop");
        if (_Logcat != null)
        {
            _Logcat.stop();
            _Logcat = null;
        }
    }

    public void log(String str)
    {
        Log.d("Unity1", str);
    }

    public void warring(String str)
    {
        Log.w("Unity1", str);
    }

    public void error(String str)
    {
        Log.e("Unity1", str);
    }
}
