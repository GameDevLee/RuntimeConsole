package com.nightgame.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleLogcat implements Runnable
{
    private IConsoleReceive _Receive;
    private volatile boolean _Running = true;

    public ConsoleLogcat(IConsoleReceive receive)
    {
        _Receive = receive;
    }

    @Override
    public void run()
    {
        try
        {
//            ArrayList<String> commandLine = new ArrayList();
//            commandLine.add("logcat");
//            commandLine.add("-d");
//            commandLine.add("-v");
//            commandLine.add("-f");
//            commandLine.add("time");
//            commandLine.add("-s");
//            commandLine.add("tag:Unity1");
//            Process process = Runtime.getRuntime().exec(commandLine.toArray(new String[commandLine.size()]));

            _Running = true;


            while (_Running)
            {
                // 清除日志
                Runtime.getRuntime().exec("logcat -c");
                String[] running = new String[]{"logcat","-s","adb logcat *: D"};
                Process process = Runtime.getRuntime().exec("logcat");
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

                String line;
                while ((line = bufferedReader.readLine()) != null)
                {
                    _Receive.OnLogReceived(line);
                }

                try
                {
                    Thread.sleep(1000L);
                }
                catch (InterruptedException e)
                {

                }
            }
        }
        catch (IOException value)
        {

        }
    }

    public void stop()
    {
        _Running = false;
    }

}
