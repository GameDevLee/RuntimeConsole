namespace LeeFramework.Console
{

    public enum ConsoleLogType
    {
        Error = 0,
        Assert = 1,
        Warning = 2,
        Log = 3,
        Exception = 4,
        Android = 5
    }
}