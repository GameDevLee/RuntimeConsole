using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LeeFramework.Console
{
    public class PlatformMgr
    {
        private AndroidJavaObject _AndroidObj;

        public PlatformMgr()
        {
            _AndroidObj = new AndroidJavaObject("com.nightgame.console.RuntimeConsole");
        }

        public void Init()
        {
            if (_AndroidObj != null)
            {
                _AndroidObj.Call("start");
            }
        }


        public void Stop()
        {
            if (_AndroidObj != null)
            {
                _AndroidObj.Call("stop");
            }
        }

        public void TestLog()
        {
            if (_AndroidObj != null)
            {
                _AndroidObj.Call("log", "TestLogllll");
            }
        }

        public void TestWarring()
        {
            if (_AndroidObj != null)
            {
                _AndroidObj.Call("warring", "TestWarringllll");
            }
        }

        public void TestError()
        {
            if (_AndroidObj != null)
            {
                _AndroidObj.Call("error", "TestErrorllll");
            }
        }
    }
}
