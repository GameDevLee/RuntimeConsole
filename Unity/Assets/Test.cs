using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LeeFramework.Console;

public class Test : MonoBehaviour
{
    public Button btnLog;
    public Button btnWarring;
    public Button btnError;

    private void Start()
    {
        Application.targetFrameRate = 60;
        btnLog.onClick.AddListener(() =>
        {
            RuntimeConsole.instance.logMgr.platformMgr.TestLog();
        });

        btnWarring.onClick.AddListener(() =>
        {
            //RuntimeConsole.instance.logMgr.platformMgr.TestWarring();
            Debug.LogWarning("Warring");
        });

        btnError.onClick.AddListener(() =>
        {
            //RuntimeConsole.instance.logMgr.platformMgr.TestError();
            RuntimeConsole.instance.OnMessage("TestAndroid");
        });
    }
}
